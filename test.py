import sst

# Components

memHierarchy_trivialCPU_3 = sst.Component("memHierarchy.trivialCPU.3", "memHierarchy.trivialCPU")
memHierarchy_trivialCPU_3.addParams({
  'uncachedRangeStart': '0x0',
  'workPerCycle': '1000',
  'uncachedRangeEnd': '0x0',
  'verbose': '1',
  'do_write': '1',
  'commFreq': '100',
  'rngseed': '7',
  'memSize': '0x100000',
  'num_loadstore': '1000',
})

memHierarchy_Cache_4 = sst.Component("memHierarchy.Cache.4", "memHierarchy.Cache")
memHierarchy_Cache_4.addParams({
  'mshr_latency_cycles': '10',
  'replacement_policy': 'lru',
  'mshr_num_entries': '64',
  'access_latency_cycles': '20',
  'cache_frequency': '2 GHz',
  'low_network_ports': '0',
  'network_bw': '5 GB/s',
  'directory_at_next_level': '1',
  'cache_line_size': '64',
  'statistics': '1',
  'L1': '0',
  'network_address': '0',
  'debug': '0',
  'idle_max': '6',
  'coherence_protocol': 'MSI',
  'high_network_ports': '1',
  'associativity': '8',
  'network_num_vc': '3',
  'debug_level': '8',
  'cache_size': '64 KB',
})

memHierarchy_Cache_3 = sst.Component("memHierarchy.Cache.3", "memHierarchy.Cache")
memHierarchy_Cache_3.addParams({
  'mshr_latency_cycles': '10',
  'replacement_policy': 'lru',
  'access_latency_cycles': '2',
  'cache_frequency': '2 GHz',
  'low_network_ports': '1',
  'network_bw': '0 GB/s',
  'directory_at_next_level': '0',
  'cache_line_size': '64',
  'statistics': '1',
  'L1': '1',
  'network_address': '0',
  'debug': '0',
  'idle_max': '6',
  'coherence_protocol': 'MSI',
  'high_network_ports': '1',
  'associativity': '4',
  'network_num_vc': '3',
  'debug_level': '8',
  'cache_size': '4 KB',
})

memHierarchy_Cache_1 = sst.Component("memHierarchy.Cache.1", "memHierarchy.Cache")
memHierarchy_Cache_1.addParams({
  'mshr_latency_cycles': '10',
  'replacement_policy': 'lru',
  'access_latency_cycles': '2',
  'cache_frequency': '2 GHz',
  'low_network_ports': '1',
  'network_bw': '0 GB/s',
  'directory_at_next_level': '0',
  'cache_line_size': '64',
  'statistics': '1',
  'L1': '1',
  'network_address': '0',
  'debug': '0',
  'idle_max': '6',
  'coherence_protocol': 'MSI',
  'high_network_ports': '1',
  'associativity': '4',
  'network_num_vc': '3',
  'debug_level': '8',
  'cache_size': '4 KB',
})

memHierarchy_Bus_1 = sst.Component("memHierarchy.Bus.1", "memHierarchy.Bus")
memHierarchy_Bus_1.addParams({
  'broadcast': '0',
  'low_network_ports': '1',
  'debug': '0',
  'idle_max': '6',
  'fanout': '0',
  'bus_frequency': '2 GHz',
  'high_network_ports': '1',
  'bus_latency_cycles': '1',
})

memHierarchy_DirectoryController_1 = sst.Component("memHierarchy.DirectoryController.1", "memHierarchy.DirectoryController")
memHierarchy_DirectoryController_1.addParams({
  'interleave_step': '0',
  'addr_range_start': '0x0',
  'network_bw': '25 GB/s',
  'statistics': '1',
  'clock': '1',
  'network_address': '1',
  'debug': '0',
  'entry_cache_size': '32768',
  'interleave_size': '0',
  'addr_range_end': '0x1F000000',
  'network_num_vc': '3',
})

memHierarchy_MemController_1 = sst.Component("memHierarchy.MemController.1", "memHierarchy.MemController")
memHierarchy_MemController_1.addParams({
  'request_width': '64',
  'backend': 'memHierarchy.simpleMem',
  'mem_size': '512',
  'divert_DC_lookups': '0',
  'direct_link_latency': '10 ns',
  'range_start': '0',
  'statistics': '0',
  'clock': '1',
  'debug': '0',
  'interleave_size': '0',
  'coherence_protocol': '0',
  'interleave_step': '0',
  'memory_file': 'N/A',
})

merlin_hr_router_1 = sst.Component("merlin.hr_router.1", "merlin.hr_router")
merlin_hr_router_1.addParams({
  'input_buf_size': '16384 b',
  'xbar_bw': '5 GB/s',
  'input_latency': '2 ns',
  'topology': 'merlin.singlerouter',
  'output_latency': '2 ns',
  'flit_size': '72 B',
  'output_buf_size': '16384 b',
  'num_ports': '2',
  'debug': '0',
  'id': '0',
  'link_bw': '5 GB/s',
})

memHierarchy_trivialCPU_4 = sst.Component("memHierarchy.trivialCPU.4", "memHierarchy.trivialCPU")
memHierarchy_trivialCPU_4.addParams({
  'uncachedRangeStart': '0x0',
  'workPerCycle': '1000',
  'uncachedRangeEnd': '0x0',
  'verbose': '1',
  'do_write': '1',
  'commFreq': '100',
  'rngseed': '7',
  'memSize': '0x100000',
  'num_loadstore': '1000',
})

# Links

Link_1 = sst.Link('Link_1')
Link_1.connect((memHierarchy_trivialCPU_3, 'mem_link', '1 ns'), (memHierarchy_Cache_1, 'high_network_0', '1 ns'))

Link_2 = sst.Link('Link_2')
Link_2.connect((memHierarchy_Cache_3, 'low_network_0', '10 ns'), (memHierarchy_Bus_1, 'high_network_1', '10 ns'))

Link_3 = sst.Link('Link_3')
Link_3.connect((memHierarchy_Cache_1, 'low_network_0', '10 ns'), (memHierarchy_Bus_1, 'high_network_0', '10 ns'))

Link_4 = sst.Link('Link_4')
Link_4.connect((memHierarchy_Bus_1, 'low_network_0', '10 ns'), (memHierarchy_Cache_4, 'high_network_0', '10 ns'))

Link_5 = sst.Link('Link_5')
Link_5.connect((memHierarchy_DirectoryController_1, 'memory', '10 ns'), (memHierarchy_MemController_1, 'direct_link', '10 ns'))

Link_6 = sst.Link('Link_6')
Link_6.connect((merlin_hr_router_1, 'port1', '10 ns'), (memHierarchy_DirectoryController_1, 'network', '10 ns'))

Link_7 = sst.Link('Link_7')
Link_7.connect((merlin_hr_router_1, 'port0', '10 ns'), (memHierarchy_Cache_4, 'directory', '10 ns'))

Link_8 = sst.Link('Link_8')
Link_8.connect((memHierarchy_trivialCPU_4, 'mem_link', '1 ns'), (memHierarchy_Cache_3, 'high_network_0', '1 ns'))

print "Done Configuring SST Model"