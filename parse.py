# Generates an output.json representing the output of the simulator.

import re
import json

f = open("output.raw", "r")

caches = []
current_cache = None

for line in f:
  line = line.strip()

  if line.startswith("--- Cache Stats"):
    current_cache = {}
    caches.append(current_cache)
  elif line.startswith("--- Directory"):
    current_cache = None

  if line.startswith("--- Name:"):
    name = line[9:].strip()
    if name == "memHierarchy.Cache.3":
      name = "L1-1"
    elif name == "memHierarchy.Cache.4":
      name = "L2"
    elif name == "memHierarchy.Cache.1":
      name = "L1-0"
    else:
      name = None

    if name != None and current_cache != None:
      current_cache["name"] = name

  if line.startswith("- "):
    key, value, *_ = line[2:].split(":")
    if current_cache != None:
      current_cache[key.strip()] = value.strip()

o = open("output.json", "w+")

hash = {}
hash["warnings"] = []
hash["errors"] = []
hash["data"] = {}
hash["data"]["caches"] = caches

o.write(json.dumps(hash))

o.close()
