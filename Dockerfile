# DOCKER-VERSION 0.11.1
FROM occam/ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /simulator
RUN echo 'y' | apt-get install autoconf
RUN echo 'y' | apt-get install openmpi-bin
RUN echo 'y' | apt-get install libboost1.53-all-dev
RUN svn checkout https://www.sst-simulator.org/svn/sst/trunk /simulator/package
RUN cd /simulator; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; /bin/bash build.sh'
CMD ['/bin/bash']
