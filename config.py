import json

data = json.load(open('input.json'))

output_file = open('experiment.py', 'w+')
output_file.write('import sst\n\n')

links = 0

def writeComponent(tag, component, module, values):
  output_file.write('%s = sst.Component("%s", "%s")\n' % (tag, component, module))
  output_file.write('%s.addParams({\n' % (tag))
  for key, value in values.items():
    if key == "identifier":
      key = "id"

    if key == "coherence_protocol" and value == "None":
      value = "0"
    elif key == "cache_size":
      value = "%s KB" % (value)
    elif key == "flit_size":
      value = "%s B" % (value)
    elif key == "input_buf_size" or key == "output_buf_size":
      value = "%s b" % (value)
    elif key == "network_bw" or key == "link_bw" or key == "xbar_bw":
      value = "%s GB/s" % (value)
    elif key == "direct_link_latency" or key == "input_latency" or key == "output_latency":
      value = "%s ns" % (value)
    elif key == "bus_frequency" or key == "cache_frequency" or key == "clock":
      value = "%s GHz" % (value)
    elif value == False:
      value = "0"
    elif value == True:
      value = "1"

    if key != "Links":
      output_file.write("  '%s': '%s',\n" % (key, value))

  output_file.write('})\n\n')

def writeLink(component_a, port_a, latency_a, component_b, port_b, latency_b):
  global links
  links = links + 1
  output_file.write("Link_%s = sst.Link('Link_%s')\n" % (links, links))
  output_file.write("Link_%s.connect((%s, '%s', '%s ns'), (%s, '%s', '%s ns'))\n\n" % (
    links,
    component_a,
    port_a,
    latency_a,
    component_b,
    port_b,
    latency_b))

output_file.write('# Components\n\n')

# Components
for k, v in data.items():
  if k == "Memory Controller":
    writeComponent('memHierarchy_MemController_1', 'memHierarchy.MemController.1', 'memHierarchy.MemController', v)
  elif k == "CPU-0":
    writeComponent('memHierarchy_trivialCPU_3', 'memHierarchy.trivialCPU.3', 'memHierarchy.trivialCPU', v)
  elif k == "CPU-1":
    writeComponent('memHierarchy_trivialCPU_4', 'memHierarchy.trivialCPU.4', 'memHierarchy.trivialCPU', v)
  elif k == "L1-0":
    writeComponent('memHierarchy_Cache_1', 'memHierarchy.Cache.1', 'memHierarchy.Cache', v)
  elif k == "L1-1":
    writeComponent('memHierarchy_Cache_3', 'memHierarchy.Cache.3', 'memHierarchy.Cache', v)
  elif k == "L2":
    writeComponent('memHierarchy_Cache_4', 'memHierarchy.Cache.4', 'memHierarchy.Cache', v)
  elif k == "Directory Controller":
    writeComponent('memHierarchy_DirectoryController_1', 'memHierarchy.DirectoryController.1', 'memHierarchy.DirectoryController', v)
  elif k == "Memory Bus":
    writeComponent('memHierarchy_Bus_1', 'memHierarchy.Bus.1', 'memHierarchy.Bus', v)
  elif k == "HR Router":
    writeComponent('merlin_hr_router_1', 'merlin.hr_router.1', 'merlin.hr_router', v)

output_file.write('# Links\n\n')

# Links
for k, v in data.items():
  link_1 = None
  link_2 = None
  if "Links" in v:
    link_hash = v["Links"]
    if "a" in link_hash:
      link_1 = link_hash["a"]
    if "b" in link_hash:
      link_2 = link_hash["b"]

  if k == "CPU-0":
    writeLink('memHierarchy_trivialCPU_3',
              'mem_link',
              link_1,
              'memHierarchy_Cache_1',
              'high_network_0',
              link_1)
  elif k == "CPU-1":
    writeLink('memHierarchy_trivialCPU_4',
              'mem_link',
              link_1,
              'memHierarchy_Cache_3',
              'high_network_0',
              link_1)
  elif k == "L1-0":
    writeLink('memHierarchy_Cache_1',
              'low_network_0',
              link_1,
              'memHierarchy_Bus_1',
              'high_network_0',
              link_1)
  elif k == "L1-1":
    writeLink('memHierarchy_Cache_3',
              'low_network_0',
              link_1,
              'memHierarchy_Bus_1',
              'high_network_1',
              link_1)
  elif k == "Directory Controller":
    writeLink('memHierarchy_DirectoryController_1',
              'memory',
              link_1,
              'memHierarchy_MemController_1',
              'direct_link',
              link_1)
  elif k == "Memory Bus":
    writeLink('memHierarchy_Bus_1',
              'low_network_0',
              link_1,
              'memHierarchy_Cache_4',
              'high_network_0',
              link_1)
  elif k == "HR Router":
    writeLink('merlin_hr_router_1',
              'port1',
              link_1,
              'memHierarchy_DirectoryController_1',
              'network',
              link_1)
    writeLink('merlin_hr_router_1',
              'port0',
              link_2,
              'memHierarchy_Cache_4',
              'directory',
              link_2)

output_file.write('print "Done Configuring SST Model"')
