import os
import subprocess

# Gather paths
scripts_path   = os.path.dirname(__file__)
simulator_path = "%s/package" % (scripts_path)
binary         = "%s/sst/core/sst" % (simulator_path)
job_path       = os.getcwd()

# Generate a experiment.py
os.system("python %s/config.py" % (scripts_path))

# Run the experiment.py
args = [binary,
        "%s/experiment.py" % (job_path)]

output_file = open('output.raw', 'w+')
print("start")
subprocess.Popen(args, stdout=output_file, stderr=output_file).wait()
print("done")
output_file.close()

# Gather results
os.system("python %s/parse.py" % (scripts_path))
