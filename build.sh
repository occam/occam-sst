cd /simulator/package
./autogen.sh
./configure --disable-silent-rules --prefix=$HOME/local/ --with-boost=$BOOST_HOME
make
make install
